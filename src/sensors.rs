use crate::Error;
use anyhow::anyhow;
use industrial_io as iio;

fn read_chan(chan: &iio::Channel) -> Result<f64, Error> {
    // TODO: actually read the channel via a buffer
    let fmt = chan.data_format();
    let scale = match fmt.with_scale() {
        true => fmt.scale(),
        false => 1.,
    };
    let raw = chan.attr_read_int("raw")?;
    let native = chan.convert(raw as i16);
    let scaled = native as f64 * scale;
    Ok(scaled)
}

pub struct Accelerometer {
    pub device: iio::Device,
    x: iio::Channel,
    y: iio::Channel,
    z: iio::Channel,
}

impl Accelerometer {
    pub fn new(device: iio::Device) -> Result<Self, Error> {
        let mut x = None;
        let mut y = None;
        let mut z = None;

        for chan in device.channels() {
            let id = match chan.id() {
                Some(i) => i,
                None => continue,
            };
            match id.as_str() {
                "accel_x" => x = Some(chan),
                "accel_y" => y = Some(chan),
                "accel_z" => z = Some(chan),
                _ => (),
            }
        }

        let mut x = x.ok_or(anyhow!("accelerometer missing x axis"))?;
        let mut y = y.ok_or(anyhow!("accelerometer missing y axis"))?;
        let mut z = z.ok_or(anyhow!("accelerometer missing z axis"))?;

        x.enable();
        y.enable();
        z.enable();

        Ok(Accelerometer { device, x, y, z })
    }

    pub fn read(&mut self) -> Result<[f64; 3], Error> {
        let x = read_chan(&self.x)?;
        let y = read_chan(&self.y)?;
        let z = read_chan(&self.z)?;

        Ok([x, y, z])
    }
}

pub struct Protractor {
    pub device: iio::Device,
    a: iio::Channel,
}

impl Protractor {
    pub fn new(device: iio::Device) -> Result<Self, Error> {
        let mut a = None;

        for chan in device.channels() {
            if chan.id().as_ref().map(String::as_str) == Some("angl") {
                a = Some(chan);
            }
        }

        let mut a = a.ok_or(anyhow!("sensor is missing angl channel"))?;
        a.enable();

        Ok(Protractor { device, a })
    }

    pub fn read(&mut self) -> Result<f64, Error> {
        read_chan(&self.a)
    }
}
